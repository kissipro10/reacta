import React, { Component,useState } from 'react';
import  PropTypes from 'prop-types';
import Dorm from './Dorm';
import Mdp from "./Mdp";
import Counter from "./Counter";
import ResultAdd from "./ResultAdd";
class App extends Component {

                constructor(){
                  super() ;
                  this.state = {
                          txt:'value by default',
                            stars:0

                  }
                }
                update(e) {
                        this.setState({txt:e.target.value});
                }

                render(){

                       return (
                           <div className="container">
                               {this.state.txt}{this.state.stars}
                               <Counter />
                               {this.props.txt}n°{this.props.stars}
                               <input type ='text' onChange={this.update.bind(this)}/>
                               <Dorm head={true}/>
                               <Mdp />
                               <ResultAdd />
                          </div>
                       )

                }


        }

        App.propTypes ={
              txt: PropTypes.string,
                stars:PropTypes.number.isRequired
        };
        App.defaultProps = {
                stars:0,
        };

        export default App;

