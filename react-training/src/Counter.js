import React,{useState} from 'react'; 

const Counter = (props) =>{
    const initialCount = 0;
    const[count,setCount]=useState(initialCount);

    return(

        <div className = "retro">
             Count: {count}
            <button className="button1" onClick={()=>setCount(initialCount)}>Reset</button>
            <br/>
            <button className="button2" onClick={()=>setCount(count +1)}>Increment </button>
            <button className="button3" onClick={()=>setCount(count -1)}>Decrement</button>

        </div>
    );
};
export default Counter