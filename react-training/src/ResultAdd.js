 import React , {Component} from 'react';
import {Col, Form, Row} from "react-bootstrap";
import axios  from './results'
class ResultAdd extends Component{
    state ={
        name:'',
        repo:'',
        poly:''

    }

    postDataHandler =(e) =>{
      e.preventDefault();
      const Data = {
          name: this.state.name,
          repo: this.state.repo,
          poly: this.state.poly
      }
      axios.post('/mark.json',Data).then(response=>{
          console.log(response)
      })
    }
    render(){
        return(
                <div className="container-fluid">

                    <Form className = "uiform" onSubmit={this.postDataHandler}>
                        <Row  className="name">
                            <Col >
                                < Form.Control   placeholder="Name" value={this.state.name}
                                                 onChange={e => this.setState({name:e.target.value})}
                                />
                            </Col>
                        </Row>
                        <br/>
                        <Row className="repo">
                            <Col>
                                < Form.Control   placeholder="Repo" value={this.state.repo}
                                 onChange={e => this.setState({repo:e.target.value})}
                                />
                            </Col>
                        </Row>
                        <br/>
                        <Row className="poly">
                            <Col>
                                < Form.Control   placeholder="Poly" value={this.state.poly}
                                                 onChange={e => this.setState({poly:e.target.value})}
                                />
                            </Col>
                        </Row>
                        <br/>
                        <button className="but">Submit</button>
                    </Form>
                </div>
        )
    }
}
export default ResultAdd